---
full_name: Iraklis Psaroudakis
image: iraklisp.png
linkedin: https://www.linkedin.com/in/kingherc/
quote: I am very proud that I have joined the organizing team   of the upcoming SoCraTes Crete 2024 unconference! <br/><br/> This is a unique opportunity to hone my organization skills, participate in one of the most exciting events on the picturesque island of Crete, to connect with like-minded, top talented people, learn about new exciting technologies and debate unconventional ideas in a welcoming and hassle-free setting environment. <br/><br/>Looking forward to it!

---