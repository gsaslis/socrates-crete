---
full_name: Iraklis Vlachakis
image: iraklisv.png
linkedin: https://www.linkedin.com/in/iraklis-vlachakis-a6941a127/
quote: As an ‘outsider’, observing the Socrates Crete open space unconference was an overwhelming experience. Having the opportunity to participate in the organising team was such an honour. Interacting with such open minded colleagues and bringing my share of knowledge to this event is an unforgettable experience. <br/><br/> I can't wait to meet everyone and all together create the best unconference ever made!

---