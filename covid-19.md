---
layout: page
title: Covid-19
description: Event policy and expectations
---

### General Event Policy and Expectations for Covid-19

Please note that the following rules are to be respected during the event:

- Masks are recommended indoors.
- Social distancing is encouraged indoors and outdoors. Please note that we will try to increase the outdoor breakout spaces as much as possible this year.
- No vaccination/Covid-19 certificates are required.
- We consider these rules to be an ethics matter and we trust that everyone understands and will comply with them.
- Check local authorities for travel and return policies.
  
Please note that the aforementioned rules are subject to modification in accordance with any regulations that the Greek government may introduce or alter until the date of the event.