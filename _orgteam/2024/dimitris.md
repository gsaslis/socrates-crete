---
full_name: Dimitris Kalouris
image: dimitris.png
linkedin: https://www.linkedin.com/in/dimitris-kalouris-5a5384223/
github: https://github.com/dkalouris
quote: Socrates Crete was a unique opportunity for me to connect in a non work environment with people that love making and talking about software in the beautiful island of Crete. <br/><br/> One of my favorite experiences was a late night "geek-out" we had where we  gathered with beers and tackled "escape room" themed puzzles using coding. I had to join the org team for this years event!

---