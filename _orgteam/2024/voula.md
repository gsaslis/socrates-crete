---
full_name: Voula Troulaki
image: voula.png
linkedin: https://www.linkedin.com/in/voula-troulaki-b0ba77180/
quote: I am really happy and excited with my participation as a member of the organization team of SoCraTes Crete 2024. <br/><br/> This team is such an inspiration for me, consisted of people having same values as trust, equality, transparency, freedom, willing to help in a spirit full of diversity and safety. I will do my best to ensure that the event meets the visitors' expectations and provides a memorable experience. <br/><br/>I can't wait for SoCraTes Crete to bring together people who care about creating good software products, just like I do.
---