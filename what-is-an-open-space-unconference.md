---
layout: page
title: What is an Open Space UNconference?
description: An... unconventional way to run a conference that - once you try - you can never go back!!
image: socrates_crete_2022_1.jpeg
---

{% include toc.html %}

## What is an Open Space Unconference?

Choose between: 

- ⏱️ executive / TL;DR version, 
- 📸 photonovela version, and 
- 🧐 academic version  



### The tl;dr version

<p>
    <span style="color: #339966;"><strong>✔ It’s all about conversations that matter</strong></span><br>
    Bring your own burning questions, listen to those of others, participate in a lot of discussion sessions and get even smarter than you already are – together!
</p>
<p>
    <span style="color: #800000;"><strong>❌ It’s not about speakers bringing presentation slides</strong></span><br>
    Nope. Please. Don’t. Seriously!<br>
    There are plenty of nice conferences to apply for as a speaker – SoCraTes Crete just isn’t one of them.
</p>

### The version with Photos

Here's what your day looks like:

- Marketplace, first thing in the morning
- Sessions
- Lunch
- Excursions
- Dinners

#### Marketplace

<p>
    1. It starts with topic suggestions at the "Marketplace". 
</p>

![Presenting a topic at an open space marketplace]({{ site.baseurl }}/assets/images/socrates_crete_marketplace_2.jpeg){: width="400" }
![Presenting a topic at an open space marketplace]({{ site.baseurl }}/assets/images/socrates_crete_2022_presenting_topic.jpeg){: width="512" }

<p>
    2. Topics are added to the "Space-Time Matrix", which acts as the 
    ad-hoc schedule for the day. 
</p>

![Adding topics to space-time matrix]({{ site.baseurl }}/assets/images/socrates_crete_space_time_matrix1.jpeg){: width="300" }
![Adding topics to space-time matrix]({{ site.baseurl }}/assets/images/socrates_crete_space_time_matrix.jpeg){: width="300" }
![The schedule for the day]({{ site.baseurl }}/assets/images/socrates_crete_space_time_matrix_schedule.jpeg){: width="300" }


#### Morning Sessions

<p>
    3. Mornings are spent discussing the topics suggested by the 
    participants during the marketplace. 
</p>

![Morning sessions]({{ site.baseurl }}/assets/images/socrates_crete_2022_2.jpeg){: width="1024" }
<br>
![Morning sessions]({{ site.baseurl }}/assets/images/socrates_crete_2022_coding_kata.jpeg){: width="1024" }
![Morning sessions]({{ site.baseurl }}/assets/images/socrates_crete_beach_session.jpeg){: width="1024" }


#### Lunch

<p>
    After the morning sessions, a delicious "light" lunch at the OAC,
    with an inspiring view to the mediterranean sea, keeps the 
    discussions going.
</p>

![A light lunch]({{ site.baseurl }}/assets/images/socrates_crete_lunch.jpeg){: width="512" }
![A light lunch]({{ site.baseurl }}/assets/images/socrates_crete_lunch_oac_view.jpeg){: width="512" }

#### Excursions

<p>
    Experience the "Sun, Sea, Sand" that Crete is famous for, emerged in
    some of the best that nature has to offer here.  
</p>

![Getting ready for the excursions]({{ site.baseurl }}/assets/images/socrates_crete_excursions1.jpeg){: width="512" }
![Getting ready for the excursions]({{ site.baseurl }}/assets/images/socrates_crete_excursions2.jpeg){: width="512" }

#### Dinners

<p>
    We close the days off with a group dinner at hand-picked restaurants. 
    We think you won't experience many work dinners like these. 
</p>

![Work continues over dinner]({{ site.baseurl }}/assets/images/socrates_crete_dinner.jpg){: width="512" }
![Work continues over dinner]({{ site.baseurl }}/assets/images/socrates_crete_dinners.jpeg){: width="512" }
![Work continues over dinner]({{ site.baseurl }}/assets/images/socrates_crete_dinner2.jpeg){: width="512" }




### The academic version


<ul>
    <li>
        An <a href="https://en.wikipedia.org/wiki/Unconference" target="_blank" rel="noopener noreferrer">unconference</a> is a <strong>participant-driven meeting</strong>. In other words: <strong>you decide</strong> what topics you want to talk about, and work on, with whom &#8211; instead of hoping a &#8220;speaker&#8221; will address that topic at least briefly.
    </li>
    <li>
        <a href="https://en.wikipedia.org/wiki/Open_Space_Technology" target="_blank" rel="noopener noreferrer">Open Space</a> (&#8220;Open Space Technology&#8221;, OST) is a specific form of an unconference that <strong>starts with an empty schedule</strong>. No times are set, no rooms are allocated, no topics are mandated, no separations made between &#8220;speakers&#8221; and &#8220;audience&#8221;. All <strong>participants work out a schedule</strong> by suggesting, planning, holding and evaluating sessions, <strong>collaboratively</strong>.
    </li>
    <li>
        Since everything is created collaboratively by the participants, obviously<strong> it matters which people feel invited, and how to &#8220;call&#8221; them together</strong>. You can find the &#8220;call&#8221; for SoCraTes Crete <a title="Who should come to SoCraTes Crete?" href="/">right on our home page</a>.
    </li>
</ul>



#### Open Space Principles

<p>
    To maximize what you can get out of the unconference, <a title="Wikipedia on Open Space" href="https://en.wikipedia.org/wiki/Open_Space_Technology" target="_blank" rel="noopener noreferrer">simple rules</a> apply. Wikipedia summarizes them nicely:
</p>

<ol id="docs-internal-guid-1a65e5b6-ba52-cf45-9c07-c5eed880c21d">
<li dir="ltr">
    <p dir="ltr">
    <strong>Whoever comes is the right people</strong><br />
    &#8230;reminds participants that they don&#8217;t need the CEO and 100 people to get something done, you need people who care. And, absent the direction or control exerted in a traditional meeting, that&#8217;s who shows up in the various breakout sessions of an Open Space meeting.
    </p>
</li>
<li dir="ltr">
    <p dir="ltr"><strong>Whenever it starts is the right time</strong><br />
    &#8230;reminds participants that spirit and creativity do not run on the clock.
    </p>
</li>
<li dir="ltr">
    <p dir="ltr"><strong>Whatever happens is the only thing that could have</strong><br />
    &#8230;reminds participants that once something has happened, it&#8217;s done—and no amount of fretting, complaining or otherwise rehashing can change that. Move on.
    </p>
</li>
<li dir="ltr">
    <p dir="ltr"><strong>When it&#8217;s over, it&#8217;s over</strong><br />
    &#8230;reminds participants that we never know how long it will take to resolve an issue, once raised, but that whenever the issue or work or conversation is finished, move on to the next thing. Don&#8217;t keep rehashing just because there&#8217;s 30 minutes left in the session. Do the work, not the time.
    </p>
</li>
<li dir="ltr">
    <strong>Wherever it happens is the right place</strong><br />
    &#8230;reminds participants that space is opening everywhere all the time. Please be conscious and aware.
</li>
</ol>

<p>Plus, the <strong>Law of Mobility</strong>:</p>


>        If at **any** time during our time together you find yourself in any situation where you are neither learning nor contributing, **go someplace else**.


_This description was originally put together to describe the [AgileCrete](https://agilecrete.org) open space unconference, [here](https://agilecrete.org/what-is-an-open-space-unconference/) and has been adopted for SoCraTes Crete which continues the AgileCrete legacy._

<style type="text/css">
    img {
        max-width: 100%;
    }
</style>