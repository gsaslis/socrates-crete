---
layout: page 
title: Frequently Asked Questions
description: FAQ concerning the event and the venue
image: socrates_crete_2022_4.jpeg
---

> The list will be populated with more questions that might arise in the upcoming days.

<a class="anchor" id="dates"></a>
<p><font size="5em"><b><a href="#dates">The website states that the SoCraTes Crete event runs from the 25th to 28th of October. Is it all 4 days?</a></b></font></p>
SoCraTes Crete runs from the evening of October 25th to early afternoon of October 28th. Accommodation packages feature check-in on the 25th and check-out on the 28th.
<hr />

<a class="anchor" id="register-sharing-room-participants"></a>
<p><font size="5em"><b><a href="#register-sharing-room-participants">How can a register for two people that both want to attend the event and share the room?</a></b></font></p>
The sign up process has to be submitted twice, each by each one participant. If you want to share a double room please contact the org team through <a href="mailto:{{ site.email }}">email</a> or <a href="{{ site.discord_url }}">our discord server</a> (See `@OrgTeam - 2024` members) and let us know with whom you are willing to share your accommodation. You can also fill these in the comments field of the registration form.
<hr />


<a class="anchor" id="register-sharing-room-non-participants"></a>
<p><font size="5em"><b><a href="#register-sharing-room-non--participants">How can I register for two people that will share a room, when only one wants to participate in the event?</a></b></font></p>
The sign up form can be submitted once. You can select the option `2. Conference + Coffee breaks + Accommodation (sharing room, double or triple for families)` for the participant and one of  `4. Accompanying adult, sharing room`, `5. Accompanying (<14 y.o.), sharing a room` or `6. Accompanying (14 y.o. - 17 y.o.), sharing a room`, as appropriate.
<hr />


<a class="anchor" id="family"></a>
<p><font size="5em"><b><a href="#family">I am planning to attend with my family. How do we book our stay at the Aquila Rithymna Beach hotel?</a></b></font></p>
For a 2 member family please check the question above.

If you are traveling with children, please note that children under 2 years of age are not required to register. But please do let us know in the comments section of the registration form, so that we can arrange for a baby cot for your room.
<br /><br />
Children up to the age of 14 have to register, but there is no additional charge.<br />
Children above the age of 14 have to register but pay a discounted price.<br />
 <br /><br />
If you want to reserve a 3-bed room please select 3 options from the registration form sharing a room.
i.e "2. Conference + Coffee breaks + Accommodation  (sharing room, double or triple for families)" + "4. Accompanying adult, sharing room" + "5. Accompanying (<14 y.o.), sharing a room".

For quadruple rooms you can simply choose one of the last 2 options (7-8) from the registration form.
<hr />

<a class="anchor" id="payment"></a>
<p><font size="5em"><b><a href="#payment">I have registered via the website. How do I pay?</a></b></font></p>
If you have successfully registered you will receive a confirmation email, including a link to the payment page. All payments are handled through OpenCollective (which we rely on for transparent finances of our non-profit entity) through [this link](https://opencollective.com/socrates-crete/events/socrates-crete-2024-fcc734e3). 
Be advised that payments are due 10 days after you will receive the email with the payment details.

Note: All bank charges are at the <b>sender’s expense</b>.
<hr />

<a class="anchor" id="refund"></a>
<p><font size="5em"><b><a href="#refund">I decided I will not attend SoCraTes Crete. Can I get a refund?</a></b></font></p>
This is depending on the date that of your pay off.

Registrations and payoffs that are executed up to 30/08/2024 can claim:
- Full refund 21 days before the event starts
- 1/3 of the amount for cancellations 14 days before the event starts
- no refund 13 days prio the event starts

Registrations and payments that are executed after to 01/09/2024 have<b>no</b> refund option.
<hr />

<a class="anchor" id="invoice"></a>
<p><font size="5em"><b><a href="#invoice">I completed the payment via bank transfer. Will I receive an invoice? When?</a></b></font></p>
Yes! You should receive an email with the payment receipt a couple of (working) days after the payment. Please do contact the Org Team if that hasn't happened!
<hr />

<a class="anchor" id="check-in-out"></a>
<p><font size="5em"><b><a href="#check-in-out">When should I check in / check out?</a></b></font></p>
Early check-in on the 25th of October might be possible. If you would like to check in even before the 25th please send us an email.

Normal check-i is after 02:00 pm and check-out before 11:00 am.
<hr />

<a class="anchor" id="her-flight"></a>
<p><font size="5em"><b><a href="#her-flight">I am flying to HER airport. How do I get to Aquila Rithymna Beach hotel?</a></b></font></p>
There is no official airport transfer planned, so participants have to make their own way from the airport to the venue.

Traditionally, though, our participants are great at self-organization, so car-sharing (whether rental or taxi) has been pretty typical in previous years. We expect something similar will also happen this year. For this, we recommend joining the [SoCraTes Crete Discord](https://discord.gg/4Z45eHvZDJ), where you can find a #carsharing channel. Expect a shared spreadsheet closer to the event dates to help coordinate this.
<hr />

<a class="anchor" id="cha-flight"></a>
<p><font size="5em"><b><a href="#cha-flight">I am flying to CHA airport. How do I get to Aquila Rithymna Beach hotel?</a></b></font></p>
See above.
<hr />

<a class="anchor" id="covid-19"></a>
<p><font size="5em"><b><a href="#covid-19">Are there any special CoViD19 measures in place for the (un)conference?</a></b></font></p>
Please check our <a href="{{ '/covid-19.html' | relative_url }}" target="_blank">CoVid19 Policy page</a>.
<hr />

<a class="anchor" id="hotel-kids"></a>
<p><font size="5em"><b><a href="#hotel-kids">I am traveling with kids. Is the accommodation at Aquila Rithymna Beach hotel suitable for us?</a></b></font></p>
Our goal for SoCraTes Crete is to be a family-friendly event. This means that rooms that feature 3 or 4 beds are spacier will be first reserved for families with children.

Venue is really suitable for kids with lots of activities indoors and outdoors, including 5 swimming pools and a location right on the beach (assuming the weather is good enough, of course).

Kids are great at self-organising stuff and they are absolutely free to do that as well!
<hr />

<a class="anchor" id="hotel-cot"></a>
<p><font size="5em"><b><a href="#hotel-cot">I am traveling with (little) kids. Is a cot offered by Aquila Rithymna Beach hotel or should we bring our own?</a></b></font></p>
A cot is offered by Aquila Rithymna Beach hotel, if you require one. Please  contact the org team through <a href="mailto:{{ site.email }}">email</a> or <a href="{{ site.discord_url }}">our discord server</a> (See `@OrgTeam - 2024` members) and let us know so that we can arrange that for you!
<hr />

<a class="anchor" id="kitchen"></a>
<p><font size="5em"><b><a href="#kitchen">Are kitchen amenities offered at Aquila Rithymna Beach hotel?</a></b></font></p>
Kitchen amenities are not provided within the rooms, apart from a small refrigerator. Aquila Rithymna Beach hotel offers excellent bars and restaurants that would happily serve your need within the day (and night).
<hr />

<a class="anchor" id="share-3-people"></a>
<p><font size="5em"><b><a href="#share-3-people">Is it possible for 3 or 4 people to share a room?</a></b></font></p>
While Aquila Rithymna Beach hotel offers rooms with 3 and 4 beds, these rooms are reserved only for families. 
<hr />

<a class="anchor" id="register-accompanying-people-share"></a>
<p><font size="5em"><b><a href="#register-accompanying-people-share">How can I register when sharing a double room?</a></b></font></p>
At the sign-up form you can select the option "2. Conference + Coffee breaks + Accommodation (sharing room, double or triple for families)" and you can also add the participant that you would like to share your room with. The registration form should look like this:<br />
<img src="assets/images/share.png" alt="sample sign up form" />
<hr />


<a class="anchor" id="register-accompanying-people-cot"></a>
<p><font size="5em"><b><a href="#register-accompanying-people-cot">How can I register for a family of 3, with a baby?</a></b></font></p>
At the sign-up form, you can select the option "2. Conference + Coffee breaks + Accommodation (sharing room, double or triple for families)", the option "4. Accompanying adult, sharing room" for the accompanying person (if they won't participate at the conference) and you can also request for a baby cot for your baby in the comments section. The registration form should look like this:<br />
<img src="assets/images/cot.png" alt="sample sign up form" />
<hr />


<a class="anchor" id="register-accompanying-people-3"></a>
<p><font size="5em"><b><a href="#register-accompanying-people-3">How can I register for a family room of 3, with a child?</a></b></font></p>
At the sign-up form, you can select the option "2. Conference + Coffee breaks + Accommodation (sharing room, double or triple for families)", the option "4. Accompanying adult, sharing room" for the accompanying person (if they won't participate at the conference) and the option "5. Accompanying (<14 y.o.), sharing a room" for a < 4 y.o kid. The registration form should look like this:<br />
<img src="assets/images/3beds.png" alt="sample sign up form" />

For families that want more comfort, there is also the option of the larger "Quadruple" rooms, which are also available for 3-person families. (see below).

<hr />


<a class="anchor" id="register-accompanying-people-4"></a>
<p><font size="5em"><b><a href="#register-accompanying-people-4">How can I register for a family room of 4?</a></b></font></p>
At the sign-up form you can select the option "7. Conference + Coffee breaks + Accommodation (Family Quadruple, single participant)" if only one person will attend the conference or the "8. Conference + Coffee breaks + Accommodation (Family Quadruple, two participants)" if both persons will attend the conference. The registration form should look like this in case that 2 persons will participate in the conference:<br />
<img src="assets/images/4beds.png" alt="sample sign up form" />
<hr />


<a class="anchor" id="what-packages-include"></a>
<p><font size="5em"><b><a href="#what-packages-include">What's included in the packages apart the accommodation and/or the conference?</a></b></font></p>
All accommodation packages include breakfast and lunch at the Aquila Rithymna Beach hotel. This means that packages except the "Conference + Coffee breaks" offer breakfast and lunch.
<br>All Conference packages include coffee breaks. This means that all participants of the SoCraTes Crete can enjoy coffee and some small snacks during a break between sessions.
<hr />

<a class="anchor" id="vegeterian-options"></a>
<p><font size="5em"><b><a href="#vegeterian-options">Is it possible to have vegetarian meals?</a></b></font></p>
Yes, it is absolutely possible to have vegetarian meals at the venue - and we will ensure there will be vegetarian options in any other dinners / events we may organize. Ahead of the event, we will request that you share your dietary preferences and we will do our best to cover them! Luckily, the Cretan mediterranean cuisine is full of vegetarian and vegan options ! 
<hr />

<a class="anchor" id="update-registration"></a>
<p><font size="5em"><b><a href="#update-registration">I have changed my mind about the registration form I completed. Can I update it?</a></b></font></p>
You can update the details in the registration form you submitted by submitting the form again using the same email as the last time. Each previous submission will be ignored!
If you want any further assistance you can contact the org team through <a href="mailto:{{ site.email }}">email</a> or <a href="{{ site.discord_url }}">our discord server</a> (See `@OrgTeam - 2024` members).
<hr />

<a class="anchor" id="first-come"></a>
<p><font size="5em"><b><a href="#first-come">I haven't confirmed my registration. Can I postpone it?</a></b></font></p>
If you have filled the registration form you have to confirm it and process the payment within 10 days after you receive the email with the payment details.

After that your reservation will be cancelled, if you haven't proceeded with the payment.

Keep in mind that hotel rooms are available on a first-come, first-served basis!
<hr />

<a class="anchor" id="cancel-registration"></a>
<p><font size="5em"><b><a href="#cancel-registration">I have registered but I won't be able to attend the event. What should I do? Can I get a refund?</a></b></font></p>
If you have registered but you won't be able to attend the event please contact us through our <a href="mailto:{{ site.email }}">email</a> by letting us know for your registration and your request for cancellation.
If you have already made the payment you can request for a refund.
All reservations that will be confirmed and paid up to the 31st of July 2024 benefit the below cancellation policy:
- Free cancellation until 21 days prior arrival
- Cancellation between 20 and 14 days prior arrival, 2 nights cancellation fees
- Cancellation from 13 days prior arrival to arrival day or No Show or early departure, 100% cancellation fees
<br /><br />
Unfortunatelly, registrations that will be confirmed after the 1st of August 2024 will have <b>no</b> option for refund.
<hr />

<a class="anchor" id="payment-fees"></a>
<p><font size="5em"><b><a href="#payment-fees">Are there any additional fees I should be aware of when making a payment?</a></b></font></p>
Yes, please note that all payment processing fees are the responsibility of the participants. 
This includes any transaction fees charged by banks, credit card companies, or payment processors. 
Ensure you factor in these additional costs when making your payment.
<hr />


<a class="anchor" id="hotel-fee"></a>
<p><font size="5em"><b><a href="#hotel-fee">How should I pay the Environmental tax?</a></b></font></p>
All accommodation packages do not include the Environmental tax. Environmental tax is €10,00 <b>per room per night</b>.
This tax should be paid directly to the hotel and should be split between the tenants of each room.
<hr />

<a class="anchor" id="after-registration"></a>
<p><font size="5em"><b><a href="#after-registration">I have submitted the registration form. What should I do next?</a></b></font></p>
* You will receive an email with details about the payment process. Please follow the link there to proceed with the payment (or see below ;) ). 
* You can join our [Discord server](https://discord.gg/4Z45eHvZDJ) to keep up to date with all the SoCraTes Crete news and meet more participants.
<hr />

<a class="anchor" id="payment-way"></a>
<p><font size="5em"><b><a href="#payment-way">I have submitted the registration form. How can I proceed to payment?</a></b></font></p>
You can pay for your registration from OpenCollective [here](https://opencollective.com/socrates-crete/events/socrates-crete-2024-fcc734e3).
<hr />

<a class="anchor" id="opencollective-tip"></a>
<p><font size="5em"><b><a href="#opencollective-tip">Should I pay the tip for OpenCollective?</a></b></font></p>
Opecollective, by default, adds a 15% tip for support. **It is absolutely NOT necessary to pay this!**
<br />

 If you are not interested to add this fee to your payment please click "edit" and select the checkbox "I don't want to contribute to Open Collective"
<br /><br />
<img src="assets/images/tip-edit-opencollective.png" alt="OpenCollective tip edit" /><br /><br />
<img src="assets/images/tip-opencollective.png" alt="OpenCollective tip" />

<hr />

<a class="anchor" id="opencollective-plans"></a>
<p><font size="5em"><b><a href="#opencollective-plans">Which plan to choose in OpenCollective, related to my registration preference?</a></b></font></p>
The plans can be found [here](https://opencollective.com/socrates-crete/events/socrates-crete-2024-fcc734e3)
<br />You need to select your plan regarding your invoicing preference and your participation preference.

| **Registration type**                                                                    | **OpenCollective plan**                                                                               |      |      |      |      |      |      |      |      |
|------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|------|------|------|------|------|------|------|------|
| Conference + Coffee breaks                                                               | Conference + Coffee breaks                                                                            |      |      |      |      |      |      |      |      |
| Conference + Coffee breaks + Accommodation (sharing room, double or triple for families) | Shared Room + Conference + Coffee breaks                                                              |      |      |      |      |      |      |      |      |
| Conference + Coffee breaks + Accommodation (single room)                                 | Single Room + Conference + Coffee breaks                                                              |      |      |      |      |      |      |      |      |
| Accompanying adult, sharing room                                                         | Can be included in one plan Double Room: 2 people, 1 Conference participant                           |      |      |      |      |      |      |      |      |
| Accompanying (14 y.o. - 17 y.o.), sharing a room                                         | Can be included in one plan Triple Room: 1 Conference participant or use the Quadruple Room - Upgrade |      |      |      |      |      |      |      |      |
| Conference + Coffee breaks + Accommodation (Family Quadruple, single participant)        | Quadruple Room + Single Conference participation                                                      |      |      |      |      |      |      |      |      |
| Conference + Coffee breaks + Accommodation (Family Quadruple, two participants)          | Quadruple Room + Double Conference participation                                                      |      |      |      |      |      |      |      |      |
|                                                                                          |                                                                                                       |      |      |      |      |      |      |      |      |

<br />

If you need to be invoiced to different vat number you need to pay one plan for each invoice.
<hr />

<a class="anchor" id="opencollective-invoicing"></a>
<p><font size="5em"><b><a href="#opencollective-invoicing">How will I receive my invoice?</a></b></font></p>
The invoice will be sent to the email address you filled on your payment.
One example of this template is the below.
<br />
<img src="assets/images/invoice-example.png" alt="Invoice Example" /><br />
You can also download the invoice after your payment, from your OpenCollective account.
<hr />

<a class="anchor" id="opencollective-invoicing"></a>
<p><font size="5em"><b><a href="#opencollective-invoicing">How can I invoice my company?</a></b></font></p>
If you need to invoice your Company you need to add the invoice details of your company on the field Legal name which is optional. Then this information will be displayed on the invoice.
This is an example for a new user. 
<br />
<img src="assets/images/legal-name.png" alt="Legal Name" /><br />
<br />
If you are already a user on OpenCollective you can edit your profile and add the Legal Name before paying for the plan.

<hr />

<a class="anchor" id="available-payment-methods"></a>
<p><font size="5em"><b><a href="#available-payment-methods">Which are the available payment methods?</a></b></font></p>
You can pay via credit card or Bank transfer.<br />
The available payments on OpenCollective are:<br />
<img src="assets/images/credit-cards.png" alt="Credit Card" /><br />
<img src="assets/images/bank-transfer.png" alt="Bank transfer" /><br />

<hr />

<a class="anchor" id="payment-fees"></a>
<p><font size="5em"><b><a href="#payment-fees">Are there any extra payment fees from registration rates?</a></b></font></p>
You will notice that the plans have a fee in OpenCollective comparing with the initial choices on registration form. This is because we increased prices by around 2% to include payment processing fees.
<hr />


<style type="text/css">
    a.anchor {
        position: relative;
        top: -75px;
    }
    img {
        max-width: 100%;
    }
</style>