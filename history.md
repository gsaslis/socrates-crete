---
layout: page
title: History
description: SoCraTes Crete continues the legacy of AgileCrete
image: socrates_crete_2022_get_together.jpeg
---

<strong>SoCraTes Crete</strong> has its roots in the <strong>AgileCrete</strong> open space event, that we started way back in 2014, when "Agile Software Development" meant a different thing that it has come to mean today.<br>

Since 2018, AgileCrete had been getting closer and closer to the rest of the Software Craft and Testing (SoCraTes) events, as we discovered we were discussing the same topics software development, product management, methodologies, psychology, systems thinking, philosophy, mental health, and many many more. 

During the last couple of iterations of AgileCrete (2018, 2019, 2022) SoCraTes Crete ran side-by-side with AgileCrete, coming closer to the international Software Craft and Testing (SoCraTes) appreciation communities.

Since 2023, SoCraTes Crete has been running as its own, standalone event, to bring us even closer to the [SoCraTes community](https://softwarecrafters.org/). 

In 2024, a new non-profit entity was founded to take on ownership of the event and underline its not-for-profit nature. You can learn more about the Software Makers association [here](https://opencollective.com/software-makers).  