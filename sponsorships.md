---
layout: page
title: Sponsorships 
description: A unique opportunity to sponsor a unique event!
image: socrates_crete_oac_view.jpeg
---

---

**When:** 24 October - 27 October 2025  
**Where:** Aquila Rithymna Beach Hotel, Crete, Greece  
**Collective:** https://opencollective.com/socrates-crete  
**Contact:** socrates-crete@proton.me  
**More:** https://socrates-crete.org  

---

![Marketplace]({{ site.baseurl }}/assets/images/socrates_crete_marketplace_2.jpeg){: width="640" style="display: block; margin: 0 auto" }

## History

>SoCraTes Crete continues the AgileCrete.org legacy

AgileCrete has always been a playground where product folks, coaches, scrum masters and developers gather to exchange and challenge ideas about all things agile since **2014**.

In **2018**, the Heraklion Software Crafters community joined this effort, adding the Software Craft and Testing (SoCraTes) flavour, in order to create a unique unconference where discussions and hack sessions will help team members better understand each other. 

In **2023**, a **new SoCraTes Crete event** is being born to bring us even closer to the SoCraTes community.

> SoCraTes Crete - An Open Space Unconference about the craft of making software, on the island of Crete.

## Become an Un(conventional)-Sponsor 

One of the main reasons people prefer unconferences, is that the lack of sponsors in most of the events make for a more authentic experience. SoCraTes Crete attendees want to get away from the traditional types of conference events, as well as the traditional types of advertising from sponsors at these events. Our Un-Sponsor packages aim to be as unobtrusive as possible. We want the companies that will support us to do so because their mentality matches our own and that of our participants.

-- That's a unique opportunity to sponsor a unique event! -- 

All finance details such as sponsors, sponsorships and expenses will be openly available through our collective in order to provide full transparency to all of our financial transaction.

## Participant Overview

For 2025, SoCraTes Crete is expecting ~60 participants with the majority working as software engineers / developers, testers / QA , and Ops / sysadmins. Based on past experience, we expect around 2/3 of this year's participants will come from Greece and 1/3 from abroad. In the past, we've had participants joining from Czech Republic, Germany, Netherlands, Poland, Sweden, Cyprus, Canada, Italy, Belgium, Denmark, Uganda, Serbia, Finland, USA, Switzerland, France, Hungary and more.

## Sponsorship Tiers

A variety of sponsorship tiers is available. Some of them are with limited availability (only 1 - so hurry up!) while others are available for multiple sponsorships. 

All sponsorships will be made through the Contributions section within the [SoCraTes Crete Collective](https://opencollective.com/socrates-crete) - in order to ensure full transparency of our finances, as a non-profit event. Sponsors can select one of the available contribution tiers on OpenCollective.

To express your interest or any further inquiry, please contact us at socrates-crete@proton.me.

### Sponsorship Tiers Comparison 

<div>

| Benefit \ Tier| Socrates | Thales | Aristotle |  Plato | Hypatia | Welcome Dinner <br/> (only 1) | Farewell Dinner <br/> (only 1) | Cretan Feast <br/> (only 1)|
| :- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| Logo on OpenCollective <span class="tooltip"> ℹ️ <span class="tooltiptext">Your logo displayed on the SoCraTes Crete 2025 page on OpenCollective</span> </span> | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Logo on website <span class="tooltip"> ℹ️ <span class="tooltiptext">Your logo on the socrates-crete.org home page, on display until December 31st 2025.</span> </span>| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | 
| Link on website <span class="tooltip"> ℹ️ <span class="tooltiptext">A rel="external" (DoFollow) link on the socrates-crete.org home page, on display until December 31st 2025.</span> </span>                     | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Social media posts <span class="tooltip"> ℹ️ <span class="tooltiptext">Your social media account and website mentioned in "Thank you" post by @socrates_crete.</span> </span> | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Brochures outside entrance <span class="tooltip"> ℹ️ <span class="tooltiptext">Your information brochure (size up to A4) on an information table in the central unconference room, throughout SoCraTes Crete 2025 event.</span> </span> | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | 
| Permalink in hall of fame <span class="tooltip"> ℹ️ <span class="tooltiptext">Permanent mention & link on our Sponsorship Hall of Fame page at socrates-crete.org and our Open Collective page.</span> </span> | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | 
| Toast to your company  <span class="tooltip"> ℹ️ <span class="tooltiptext">A toast to your company's good name during the dinner, and mentions in announcements prior to the dinner itself.</span> </span> | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | 
| Photo of toast posted on social media <span class="tooltip"> ℹ️ <span class="tooltiptext">A photo of the toast to your company's good name during the dinner posted to our social media, for your team to repost.</span> </span>| ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ |
| Amount | €100 | €200 | €300 | €500 | €800 | €1300 | €2100 | €3400 |

</div>


## Expenses

As a group of **volunteers** who help organise the event, we make no profit. SoCraTes Crete itself receives no money. The event is entirely **funded by its participants**, and all money goes directly to our venue, to cover for meals, catering, the conference rooms and facilities, accommodation, etc. With no profit, we cannot afford to give much back to the participants. 

-- **This is where you come in!** -- 

Our sponsorships cover expenses that will allow each participant to have some more **great moments** to remember!

Welcome dinner, farewell dinner freebies such as T-shirts and of course a Cretan feast are definitely something that most participants will reminisce later!

Through sponsorships we will try to cover the following expenses:

* **Freebies**
* **Welcome Dinner**
* **Farewell Dinner**
* **Traditional Cretan Night**

### Freebies

As participants expect some kind of freebies from an event we do not want to disappoint them. We will try to cover the expenses for the “usual” freebies you would expect from a conference. Of course, since SoCraTes Crete is an unconference, they will be a little **un-usual**!

### Welcome Dinner

On the night before the official SoCraTes Crete activities start, everyone is welcomed on the island, with some of the best Crete has to offer. Its food!

A large, delicious, **mouth-watering Cretan-style dinner**, based largely on the fresh vegetables that one can easily get hold of, on the island, both in high quality, as well as in abundance. 

![Welcome Dinner]({{ site.baseurl }}/assets/images/socrates_crete_dinners.jpeg){: width="640" style="display: block; margin: 0 auto" }

> Isn’t this kind of awareness for your company much more engaging than a plain old roll-up stand up that you would post up, for everyone to walk past and ignore? 

### Farewell Dinner

One last night with the delicious Cretan food, with the ample portions, **intense flavours** from the high quality **raw ingredients** and the refreshing wine that lightens the mood.

The farewell dinner is the last opportunity to talk to people you didn’t get a chance to in the previous days, discover new collaboration / job opportunities, or simply argue about the importance of life, the universe and everything. 

![Farewell Dinner]({{ site.baseurl }}/assets/images/socrates_crete_dinner2.jpeg){: width="640" style="display: block; margin: 0 auto" }

> Think of the night before you go home from an amazing holiday with a good group of friends. This, is what the farewell dinner is.

### Cretan Night

If you’ve ever happened to attend a Cretan feast, you will know it’s a **night to remember**!

Crete has its own traditional music that is different from the rest of Greece, and all the Cretans – young & old alike – are very much into this type of **entertainment**. Cretan music always accompanies the different Cretan dances, so the feasts are always held outdoors (remember, this is the Cretan summer we’re talking about) and there is always a big space in the middle to allow for a **big circle of people** to dance to the music.

We want to bring this experience to the SoCraTes Crete participants, sponsorship would cover the costs for the **Cretan feast** plus the cost for a small **Cretan band** to entertain our guests!

![Cretan Night]({{ site.baseurl }}/assets/images/socrates_crete_2022_cretan_night.jpg){: width="640" style="display: block; margin: 0 auto" }

> Everyone is welcome to join in and Cretans very much appreciate and take pride in teaching tourists how to dance!

## How to sponsor

All sponsorships will be made through the [Contributions section within the SoCraTes Crete Collective](https://opencollective.com/socrates-crete#category-CONTRIBUTE) by selecting each one of the available contribution tiers. More details about the Fincancial contributions can be found at the [Open Collective Documentations](https://docs.opencollective.foundation/how-it-works/financial-contributions), [the financial contributions flow and](https://docs.opencollective.com/help/financial-contributors/payments#financial-contribution-flow) the [FAQ page](https://docs.opencollective.foundation/faq/contributions-faq).

For any assistance, please contact us at `socrates-crete@proton.me`. If you have any inquiry or proposal we’d be very **happy to discuss** it with you!

Your company will cover a pre-set amount that goes towards the bill, so that you don’t have to worry about how many people attend. The remainder of the bill will be equally split between everyone else.

<style type="text/css" rel="stylesheet">
img {
    max-width: 100%;
}

/* Tooltip container */
.tooltip {
  position: relative;
  display: inline-block;
}

/* Tooltip text */
.tooltip .tooltiptext {
  visibility: hidden;
  width: 260px;
  background-color: black;
  opacity: 80%;
  color: #fff;
  text-align: center;
  padding: 5px;
  border-radius: 6px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 1;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>
