---
full_name: Yorgos Saslis
image: yorgos.jpg
website: https://gsaslis.github.io
linkedin: https://www.linkedin.com/in/gsaslis
mastodon: https://chaos.social/@yorgos
github: https://github.com/gsaslis
gitlab: https://gitlab.com/gsaslis
quote: As a co-founder of AgileCrete, back in 2014, and SoCraTes Crete since 2018, I am amazed how much I am still learning, with every new iteration - 10 years down the line. <br/><br/> I'm really looking forward to another year of "burning questions" on the Software making Craft. <br/><br/> I am committed to providing a safe, inclusive space that inspires collaboration and knowledge sharing. 
---