---
full_name: Mihalis Zampetakis
image: mihalis.png
website: https://mzampetakis.com/
linkedin: https://www.linkedin.com/in/mzampetakis/
github: https://github.com/mzampetakis
quote: Having experienced the transformative power of SoCraTes open space unconferences over the past few years, I decided to join the organizing team for the upcoming event here in Crete! <br/><br/> These events have not only advanced me professionally but have also impacted me personally. By joining the organizing team, I will assist to the preparation of the event and also to ensure that more individuals have the opportunity to benefit from the unique and enriching experience that SoCraTes open space unconference offers. <br/><br/> Looking forward for SoCraTes Crete 2024!

---