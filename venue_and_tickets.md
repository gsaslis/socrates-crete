---
layout: page 
title: Tickets and Venue 
description: Registrations for 2025 will open soon - stay tuned! 
image: venue-sun-pool.jpg
---


<h2>SoCraTes Crete 2025</h2>

<p>After 4 iterations of AgileCrete, 4 iterations of AgileCrete + SoCraTes Crete, SoCrates Crete came back in 2024 in Rethymno, Crete!! 🎉&nbsp;🥳&nbsp;🚀</p>

<p><b>SoCraTes Crete 2025</b> will take place in the same <b>amazing location</b> as 2024!</p>


<p>From 24/10 to 27/10 2025 we will meet, interact, discuss and have fun, in aamazing location in Rethymno. With its charming Venetian Old Town, its colorful market and beautiful beach stretching along its coastline, Rethymno is the perfect scenario for this year’s edition.</p>

<h2>A Retreat in a Dream Setting</h2>

<p>Located in Adelianos Kampos, few minutes away from the beautiful city of Rethymno, Aquila Rythimna beach will be our host for SoCraTes Crete 2025. In a beautiful setting the beachfront resort offers all comforts possible for our unconference: great indoor and outdoor facilities, exquisite restaurants, cozy rooms, pools and much much more. Special mention to the dedicated facilities of the kids club, providing an exciting program for the little ones while the big ones are connecting!</p>

<p><a href="https://rithymnabeach.com/">Aquila Rithymna Beach</a> hotel will allow SoCraTes Crete to function as the perfect <strong>retreat</strong> while keeping characteristics you’ve all enjoyed in the previous years.</p>

![pool]({{ site.baseurl }}/assets/images/aquila_pool.jpg){: width="1024" }
<p>Small taste from coder's heaven!</p>

<h2>Accommodation</h2>

<p>All of our participants are offered the opportunity to book their stay at the event venue, ensuring ample interaction among attendees and encouraging a deeper exchange of knowledge. Please note that rooms are available on a first-come, first-served basis!</p>

<p>Seaside Serenity: Embrace the Beauty of Aegean</p>

![beach]({{ site.baseurl }}/assets/images/aquila_beach.jpg){: width="1024" }

<p>
<strong>Alternatively</strong>, guests can book their participation only for the un-conference, ensuring in any case their presence during all the activities that will be taking place in Aquila Rythimna Beach.
</p>

<h3><a id="kids-club"></a>Kids Club</h3>
This year, we've booked into kids activities that will happen in parallel to our open space sessions! 
These include lunch and also dancing, football, facepainting, ice-cream, movies and many more. They will run daily except Sunday, but stay tuned, we're hoping to change that too!
<!-- <br /> You can view the full activities schedule: <a href="./assets/ΚΡΙ ΚΡΙ 10.00-18.00.pdf">here</a>. 
<br /> For more information you can also check out : <a href="https://rithymnabeach.com/kids-only#1495989638802-fe0140b9-8366">here</a>.  -->

![Full Packages]({{ site.baseurl }}/assets/images/aquila_playground.jpg){: width="1024" }

<h3><a id="full-packages"></a>Available Packages</h3>
<p>
Available packages will be available soon. In the mean time <a href="https://forms.zohopublic.eu/socratescrete/form/Newsletter/formperma/_Opjp5lY-xBvtZDmen3iMj-bAx2EzEXDE70zsCOINgo" target="_blank">stay tunned with our latest exciting news</a>.
</p>

<!-- There are plenty of options to select for SoCraTes Crete 2024 registration. The available packages are:

<br />
1. Conference + Coffee breaks: 105 €
<br />
2. Conference + Coffee breaks + Accommodation (sharing room): 285 €
<br />
3. Conference + Coffee breaks + Accommodation (single room): 345 €
<br />
4. Accompanying adult, sharing room: 150 €
<br />
5. Accompanying (<14 y.o.), sharing a room: 0 €
<br />
6. Accompanying (14 y.o. - 17 y.o.), sharing a room: 90 €
<br />
7. Conference + Coffee breaks + Accommodation (Family Quadruple, single participant): 540 €
<br />
8. Conference + Coffee breaks + Accommodation (Family Quadruple, two participants): 645 €
<br /><br />
<span style="color:#AAAAAA;font-size:0.9em;"> All accommodation packages (2-8) include breakfast and lunch at the Aquila Rithymna Beach hotel. The above rates do not include the Environmental tax, €10,00 per room per night, which will be paid directly to the hotel.</span>
<br /><br />
For any inquiry please visit our <a href="/faq">FAQ page</a>.
</p>

<h2><a id="how-to-book"></a>How to Book</h2>

<p>
Registrations have been closed for 2024. Stay tunned for the 2025 event!
<br /><br />

You can contact us for any further information, requests, how to donate and much more <a  target="_blank" href="https://discord.gg/4Z45eHvZDJ">join us on Discord</a> or  <a  target="_blank" href="https://forms.zohopublic.eu/socratescrete/form/Newsletter/formperma/_Opjp5lY-xBvtZDmen3iMj-bAx2EzEXDE70zsCOINgo"> join our Newletter </a> for the latest news.
<br /> -->

For now, save the date, spread the word and get ready to embark on an adventure like no other! Let's make memories, forge connections, and create together the best un-convention ever. 

![night]({{ site.baseurl }}/assets/images/aquila_night.jpg){: width="1024" max-width=100% }

<h2>Alternatives</h2>

<p>
Apart from the Aquila Rithymna Beach hotel, Adelianos Kampos also offers other resorts, and some rooms you can find on platforms like AirBnB or Booking.com, etc.
</p>

<p>
Taking a bus to the hotel from the town of Rethymno is certainly an option, and while buses are frequent 
we’d still recommend hiring / sharing a car instead!
</p>
> To maximize your experience at the SoCraTes Crete Open Space Unconference, we highly recommend staying at the same venue where the event will be held. This will ensure convenience, easy access to the event, networking opportunities, and the ability to fully immerse yourself in the conference experience without any distraction.

<p>
    By the way: <strong>Participants traditionally share cars</strong>, enjoy giving others a ride to the venue and are a lovely bunch of helpful individuals! <a href="mailto:{{site.email}}" target="_blank">Drop us an email,</a> or <a href="{{site.discord_url}}" target="_blank">join us on Discord</a>!
</p>

<style type="text/css">
img {
    max-width: 100%;
}
</style>
