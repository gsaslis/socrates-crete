# SoCraTes Crete Website

This repo is a Jekyll base theme the is used to produce the static website of SoCraTes Crete. The site is currently served under [https://socrates-crete.org/](https://socrates-crete.org/)

# About SoCraTes Crete

SoCraTes Crete is  an Open Space Unconference about the craft of making software, on the island of Crete.

## How to test locally

Clone this repository locally.
### MacOS

0. You need to have Ruby [installed](https://www.ruby-lang.org/en/documentation/installation/)
1. `gem install bundler`
2. `bundle install`
3. `bundle exec jekyll serve`
4. the webpage can be accessed at http://localhost:4000
5. changes on the webpage sources trigger an automatic rebuild (just re-load the page in the browser)

In case you get "invalid byte sequence in US-ASCII. Bundler cannot continue" error when trying above commands in linux run
0. `export LANG="en_US.UTF-8"`
1. `export LC_ALL="en_US.UTF-8"`
3. `bundle exec jekyll serve`

### Linux and others

1. install the Docker client (follow the procedure for your Linux distribution or operating system)
2. clone the website repository `git clone git@gitlab.com:socrates-crete/site.git`
3. in the root of the repository create the following directories
    ```
    cd site/
    mkdir -p vendor/bundle
    ```
4. run Jekyll in a Docker container to build and serve the website locally (amend the local port, if already a service is listening on that port on your computer)
    ```
    docker run --rm -it \
     --volume="$PWD:/srv/jekyll" \
     --volume="$PWD/vendor/bundle:/usr/local/bundle" \
     --publish 4000:4000 jekyll/jekyll:4.2.0 \
     jekyll serve
    ```
5. the webpage can be accessed at http://localhost:4000
6. changes on the webpage sources trigger an automatic rebuild (just re-load the page in the browser)

# Contributing

If you would like to report a bug or request a feature, feel free to do so by opening an issue in [the GitLab repository](https://gitlab.com/socrates-crete/site/-/issues) and we will be more than happy to help!
Alternatively, you can directly contribute to the main repo by opening a Merge Request, but it's highly recommended to open an issue to discuss about it first.

# Credits

A Jekyll version of the "Spectral" theme by [HTML5 UP](https://html5up.net/).

Repository [Jekyll logo](https://github.com/jekyll/brand) icon licensed under a [Creative Commons Attribution 4.0 International License](http://choosealicense.com/licenses/cc-by-4.0/).
