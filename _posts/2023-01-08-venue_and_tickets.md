---
 layout:     redirect
 redirect:   /venue_and_tickets.html
 title: Tickets and Venue 
 description: Registrations for 2025 will open soon - stay tuned! 
 image: venue-sun-pool.jpg
---
