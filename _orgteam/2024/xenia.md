---
full_name: Xenia Papadia
image: xenia.png
linkedin: https://www.linkedin.com/in/polyxenipapadia/
quote: I’m thrilled to be a member of the organizing committee for SoCraTes Crete. I’ve always believed that the best ideas come from open, dynamic conversations, which is why the Open Space UNconference format of SoCraTes excites me so much. It brings together diverse minds to share experiences, tackle real-world challenges, and explore new perspectives in an unstructured, creative environment. Being part of this community-driven event allows me to connect with others who are equally passionate about learning and improving—whether in software development, product management, or beyond. <br/><br/> I’m excited to help facilitate an event where we can all grow together, and I’m looking forward to the thought-provoking discussions that will shape our future projects and ideas! 
---